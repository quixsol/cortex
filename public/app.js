// Connect with server
const socket = io('http://localhost:8080');
let username

function updateArrowPosition(data) 
{
	arrow = document.getElementById(data.id);
	arrow.style.left = `${data.x}px`;
	arrow.style.top = `${data.y}px`;
}

function updateClientsPointers(data) 
{
	for (var key of Object.keys(data)) 
	{
		if(key != username)
		{ // all points excep my point
			point = data[key];
			point.id = key;
			updatePointer(point);
			updateArrowPosition(point);
		}
	}
}

function updatePointer(point) 
{	
	var element = document.getElementById(point.id);

	if(typeof(element) == 'undefined' || element == null) // check if the element already exist
	{
		var randomColor = Math.floor(Math.random()*16777215).toString(16);

		clientArrow = document.createElement('div');
		clientArrow.setAttribute("id", point.id);
		clientArrow.setAttribute("class", 'arrow');
		clientArrow.innerHTML = point.id;
		clientArrow.style.backgroundColor = "#"+randomColor;
		document.body.appendChild(clientArrow)
	}
}

document.addEventListener('mousemove', (event) => {
	const coordinates = {
		x: event.clientX,
		y: event.clientY,
		id: -1
	};
	// update my coordination
	updateArrowPosition(coordinates);

	// update all other clients about my new position
	socket.emit('updatePostion', { x: coordinates.x, y: coordinates.y, id: username });
});

// listen to clients position update
socket.on('updatePostion', (data) => {
	updateArrowPosition(data);
});

// get all clients pointers when new client connected
socket.on('updatePointers', (data) => {
	updateClientsPointers(data);
});

// remove client pointer when he disconnected
socket.on('clientLeft', (id) => {
	document.getElementById(id).remove();
});

// update pointer name label when client connected
socket.on('updateName', (name) => {
	username = name;
	document.getElementById("-1").innerHTML = username;
});



