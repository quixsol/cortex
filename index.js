var express = require('express');
const session = require('express-session');
const bodyParser = require('body-parser');
const path = require('path');
const socketio = require('socket.io');

// App Setup
const app = express();

let connectedClients = {};

app.use(express.static('public'));

app.use(session({
	secret: 'secret',
	resave: true,
	saveUninitialized: true
}));

app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());

app.get('/board', function(request, response) 
{
	if(request.session.loggedin)
	{
		const io = require('socket.io')(server, { origins: '*:*'});
		const username = request.session.username;
	
		io.on('connection', (socket) => {
			const socketid = socket.id;
			let client = {[username]: {x:0, y:0}};
			// update pointer name
			socket.emit('updateName', username);
			// update all other connected clients
			socket.broadcast.emit('updatePointers', client);
			// add client to connected clients array
			connectedClients[username] = {x:0,y:0};
			// update the new connected client about all other connected clients
			socket.emit('updatePointers', connectedClients);

			// client mouse tracker listener
			socket.on('updatePostion', (data) => {
				// update client position
				socket.broadcast.emit('updatePostion', data);
				// update last position in array
				connectedClients[username] = {x:data.x,y:data.y};
			});

			socket.on('disconnect', () => {
				// wait 5 sec before update other clients that client just left
				const timeoutObj = setTimeout(() => {
					// remove client from array
					delete connectedClients[username];
					// Update all clients about this event
					socket.broadcast.emit('clientLeft', username);
				  }, 5000);
			});
		});
		// render board
		response.sendFile(path.join(__dirname + '/public/board.html'));
	} 
	else 
	{
		// render login page
		response.sendFile(path.join(__dirname + '/public/index.html'));
	}
});

app.post('/auth', function(request, response) {
	var username = request.body.username;
	var password = request.body.password;
	// make sure the user insert his user name and the password correct
	if (username && password == '123456') {
		request.session.loggedin = true;
		request.session.username = username;
		response.redirect('/board');
	}
});

const server = app.listen(8080, () => {
	console.log('Listening to requests on port - 8080');
});
